﻿
// ImageFiltering.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CImageFilteringApp:
// Сведения о реализации этого класса: ImageFiltering.cpp
//

class CImageFilteringApp : public CWinApp
{
public:
	CImageFilteringApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CImageFilteringApp theApp;
