﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется ImageFiltering.rc
//
#define IDD_IMAGEFILTERING_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_ZERO                        1000
#define IDC_INTERPOLATION               1001
#define IDC_WIDTH                       1004
#define IDC_HEIGHT                      1005
#define IDC_LINE                        1008
#define IDC_LOGARITHM                   1009
#define IDC_EDITNOISE                   1010
#define IDC_EDITFILTER                  1011
#define IDC_EDITSKO                     1012
#define IDC_CALCULATE                   1013
#define IDC_DISCHARGE                   1014
#define IDC_DRAWIMAGE                   1015
#define IDC_IMAGE                       1017
#define IDC_NOISE                       1018
#define IDC_SPECTRUM                    1019
#define IDC_FILTER                      1020
#define IDC_DRAWNOISE                   1021
#define IDC_DRAWFILTER                  1022
#define IDC_DRAWSPECTRUM                1023
#define IDC_CIRCLE                      1026
#define IDC_RECTANGLE                   1027
#define IDC_MODEL                       1034
#define IDC_RADIO6                      1035
#define IDC_PICTURE                     1035

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1036
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
