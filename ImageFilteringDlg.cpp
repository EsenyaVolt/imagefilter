﻿
// ImageFilteringDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "ImageFiltering.h"
#include "ImageFilteringDlg.h"
#include "afxdialogex.h"
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CImageFilteringDlg

#define DOTSIMAGE(x,y) (xpImage*((x)-xminImage)),(ypImage*((y)-ymaxImage)) 
#define DOTSNOISE(x,y) (xpNoise*((x)-xminNoise)),(ypNoise*((y)-ymaxNoise))
#define DOTSSPECTRUM(x,y) (xpSpectrum*((x)-xminSpectrum)),(ypSpectrum*((y)-ymaxSpectrum))
#define DOTSFILTER(x,y) (xpFilter*((x)-xminFilter)),(ypFilter*((y)-ymaxFilter))


CImageFilteringDlg::CImageFilteringDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IMAGEFILTERING_DIALOG, pParent)
	, value_width(300)
	, value_height(300)
	, value_noise(10)
	, value_filter(90)
	, value_SKO(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CImageFilteringDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MODEL, radio_model);
	DDX_Control(pDX, IDC_PICTURE, radio_image);
	DDX_Text(pDX, IDC_WIDTH, value_width);
	DDX_Text(pDX, IDC_HEIGHT, value_height);
	DDX_Control(pDX, IDC_ZERO, radio_zero);
	DDX_Control(pDX, IDC_INTERPOLATION, radio_interpolation);
	DDX_Control(pDX, IDC_LINE, radio_line);
	DDX_Control(pDX, IDC_LOGARITHM, radio_logarithm);
	DDX_Control(pDX, IDC_RECTANGLE, radio_rectangle);
	DDX_Control(pDX, IDC_CIRCLE, radio_circle);
	DDX_Text(pDX, IDC_EDITNOISE, value_noise);
	DDX_Text(pDX, IDC_EDITFILTER, value_filter);
	DDX_Text(pDX, IDC_EDITSKO, value_SKO);
}

BEGIN_MESSAGE_MAP(CImageFilteringDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CALCULATE, &CImageFilteringDlg::OnBnClickedCalculate)
	ON_BN_CLICKED(IDC_DISCHARGE, &CImageFilteringDlg::OnBnClickedDischarge)
	ON_BN_CLICKED(IDC_DRAWFILTER, &CImageFilteringDlg::OnBnClickedDrawfilter)
	ON_BN_CLICKED(IDC_DRAWSPECTRUM, &CImageFilteringDlg::OnBnClickedDrawspectrum)
	ON_BN_CLICKED(IDC_DRAWNOISE, &CImageFilteringDlg::OnBnClickedDrawnoise)
	ON_BN_CLICKED(IDC_DRAWIMAGE, &CImageFilteringDlg::OnBnClickedDrawimage)
END_MESSAGE_MAP()


// Обработчики сообщений CImageFilteringDlg

BOOL CImageFilteringDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	//для  картинки
	PicWndImage = GetDlgItem(IDC_IMAGE);
	PicDcImage = PicWndImage->GetDC();
	PicWndImage->GetClientRect(&PicImage);

	PicWndNoise = GetDlgItem(IDC_NOISE);
	PicDcNoise = PicWndNoise->GetDC();
	PicWndNoise->GetClientRect(&PicNoise);

	PicWndSpectrum = GetDlgItem(IDC_SPECTRUM);
	PicDcSpectrum = PicWndSpectrum->GetDC();
	PicWndSpectrum->GetClientRect(&PicSpectrum);

	PicWndFilter = GetDlgItem(IDC_FILTER);
	PicDcFilter = PicWndFilter->GetDC();
	PicWndFilter->GetClientRect(&PicFilter);

	radio_model.SetCheck(TRUE);
	radio_interpolation.SetCheck(TRUE);
	radio_line.SetCheck(TRUE);
	radio_circle.SetCheck(TRUE);

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CImageFilteringDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CImageFilteringDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//Функция отрисовки Куполов
void CImageFilteringDlg::DrawGauss(vector<vector<DotsDome>> vec, CDC* WinDc, CRect WinxmaxGraphc)
{
	//ГРАФИК СИГНАЛА
	double GU_X = value_width / 2.;
	double GU_Y = value_height / 2.;
	xminImage = -GU_X * 1.01;
	xmaxImage = GU_X * 1.01;
	yminImage = -GU_Y * 1.01;			//минимальное значение y
	ymaxImage = GU_Y * 1.01;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width();
	double heightY = WinxmaxGraphc.Height();
	xpImage = (widthX / (xmaxImage - xminImage));			//Коэффициенты пересчёта координат по Х
	ypImage = -(heightY / (ymaxImage - yminImage));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(0, 0, 0));

	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			double xxi = vec[i][j].x;
			double yyi = vec[i][j].y;
			int color = vec[i][j].color;

			MemDc->MoveTo(DOTSIMAGE(xxi, yyi));
			MemDc->FillSolidRect(DOTSIMAGE(xxi, yyi), 5, 5, RGB(color, color, color));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

//Функция отрисовки Куполов+шума
void CImageFilteringDlg::DrawGaussNoise(vector<vector<DotsDome>> vec, CDC* WinDc, CRect WinxmaxGraphc)
{
	//ГРАФИК СИГНАЛА
	double GU_X = value_width / 2.;
	double GU_Y = value_height / 2.;
	xminNoise = -GU_X * 1.01;
	xmaxNoise = GU_X * 1.01;
	yminNoise = -GU_Y * 1.01;			//минимальное значение y
	ymaxNoise = GU_Y * 1.01;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width();
	double heightY = WinxmaxGraphc.Height();
	xpNoise = (widthX / (xmaxNoise - xminNoise));			//Коэффициенты пересчёта координат по Х
	ypNoise = -(heightY / (ymaxNoise - yminNoise));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(0, 0, 0));

	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			double xxi = vec[i][j].x;
			double yyi = vec[i][j].y;
			int color = vec[i][j].color;

			MemDc->MoveTo(DOTSNOISE(xxi, yyi));
			MemDc->FillSolidRect(DOTSNOISE(xxi, yyi), 5, 5, RGB(color, color, color));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

//Функция отрисовки Спектра
void CImageFilteringDlg::DrawGaussSpectrum(vector<vector<DotsDome>> vec, vector<double> Coord, CDC* WinDc, CRect WinxmaxGraphc)
{
	//ГРАФИК СИГНАЛА
	if (radio_zero.GetCheck() == BST_CHECKED)
	{
		double GU_X = value_width / 2.;
		double GU_Y = value_height / 2.;

		int newSize_x = NumberDegreeTwo(value_width);
		int newSize_y = NumberDegreeTwo(value_height);

		int differenceSize_x = abs(newSize_x - value_width);
		int differenceSize_y = abs(newSize_y - value_height);

		xminSpectrum = -GU_X * 1.03;
		xmaxSpectrum = (GU_X + differenceSize_x) * 1.015;
		yminSpectrum = -GU_Y * 1.07;			//минимальное значение y
		ymaxSpectrum = (GU_Y + differenceSize_y) * 1.01;
	}

	if (radio_interpolation.GetCheck() == BST_CHECKED)
	{
		double GU_X = value_width / 2.;
		double GU_Y = value_height / 2.;

		xminSpectrum = -GU_X * 1.01;
		xmaxSpectrum = GU_X * 1.01;
		yminSpectrum = -GU_Y * 1.01;			//минимальное значение y
		ymaxSpectrum = GU_Y * 1.01;
	}

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width();
	double heightY = WinxmaxGraphc.Height();
	xpSpectrum = (widthX / (xmaxSpectrum - xminSpectrum));			//Коэффициенты пересчёта координат по Х
	ypSpectrum = -(heightY / (ymaxSpectrum - yminSpectrum));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(0, 0, 0));

	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			double xxi = vec[i][j].x;
			double yyi = vec[i][j].y;
			int color = vec[i][j].color;

			MemDc->MoveTo(DOTSSPECTRUM(xxi, yyi));
			MemDc->FillSolidRect(DOTSSPECTRUM(xxi, yyi), 5, 5, RGB(color, color, color));
		}
	}

	if (radio_circle.GetCheck() == BST_CHECKED && !Coord.empty())
	{
		CPen brush;
		brush.CreateStockObject(HOLLOW_BRUSH);

		CPen circle_pen;
		circle_pen.CreatePen(		//для сетки
			PS_DASHDOTDOT,				//сплошная линия
			1,						//толщина 3 пикселя
			RGB(255, 0, 100));			//цвет white

		MemDc->SelectObject(&circle_pen);
		MemDc->SelectObject(&brush);

		MemDc->Ellipse(DOTSSPECTRUM(Coord[0], Coord[1]), DOTSSPECTRUM(Coord[2], Coord[3]));
	}

	if (radio_rectangle.GetCheck() == BST_CHECKED && !Coord.empty())
	{
		CPen brush;
		brush.CreateStockObject(HOLLOW_BRUSH);

		CPen rectangle_pen;
		rectangle_pen.CreatePen(		//для сетки
			PS_DASHDOTDOT,				//сплошная линия
			1,						//толщина 3 пикселя
			RGB(0, 255, 255));			//цвет white

		MemDc->SelectObject(&rectangle_pen);
		MemDc->SelectObject(&brush);

		MemDc->Rectangle(DOTSSPECTRUM(Coord[0], Coord[1]), DOTSSPECTRUM(Coord[2], Coord[3]));
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

//Функция отрисовки Спектра
void CImageFilteringDlg::DrawGaussFilter(vector<vector<DotsDome>> vec, CDC* WinDc, CRect WinxmaxGraphc)
{
	//ГРАФИК СИГНАЛА
	double GU_X = value_width / 2.;
	double GU_Y = value_height / 2.;
	xminFilter = -GU_X * 1.01;
	xmaxFilter = GU_X * 1.01;
	yminFilter = -GU_Y * 1.01;			//минимальное значение y
	ymaxFilter = GU_Y * 1.01;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width();
	double heightY = WinxmaxGraphc.Height();
	xpFilter = (widthX / (xmaxFilter - xminFilter));			//Коэффициенты пересчёта координат по Х
	ypFilter = -(heightY / (ymaxFilter - yminFilter));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(0, 0, 0));

	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			double xxi = vec[i][j].x;
			double yyi = vec[i][j].y;
			int color = vec[i][j].color;

			MemDc->MoveTo(DOTSFILTER(xxi, yyi));
			MemDc->FillSolidRect(DOTSFILTER(xxi, yyi), 5, 5, RGB(color, color, color));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

//Функция рассчета компонента Куполов
double CImageFilteringDlg::GaussComponent(double x, double y, double A, double mathX, double mathY, double disp)
{
	//компонента
	double s = 0;

	//вспомогательные величины
	double help1 = -((x - mathX) * (x - mathX));
	double help2 = -((y - mathY) * (y - mathY));

	s = A * exp((1 / (disp * disp)) * (help2 + help1));

	return s;
}

//Функция расчета Куполов
vector<vector<DotsDome>> CImageFilteringDlg::GaussDome(vector<double> parameter)
{
	vector<vector<DotsDome>> Gauss;
	vector<DotsDome> GaussHelp;
	DotsDome help;
	//размер модели
	int sizeX = value_width;
	int sizeY = value_height;
	//половина модели(ГУ)
	double GU_X = value_width / 2.;
	double GU_Y = value_height / 2.;

	//амплитуда
	double A = parameter[0];
	//мат.ожидание
	double mathX = parameter[1];
	double mathY = parameter[2];
	//дисперсия
	double disp = parameter[3];

	//считаем купол
	for (int i = 0; i < sizeX; i++)
	{
		help.x = i - GU_X;
		for (int j = 0; j < sizeY; j++)
		{
			help.y = j - GU_Y;
			help.value.real = GaussComponent(help.x, help.y, A, mathX, mathY, disp);
			GaussHelp.push_back(help);
		}

		Gauss.push_back(GaussHelp);
		GaussHelp.clear();
	}

	return Gauss;
}

//Функция отрисовки Куполов
vector<vector<DotsDome>> CImageFilteringDlg::GaussCalculate()
{
	vector<vector<DotsDome>> Gauss;
	vector<vector<DotsDome>> GaussHelp;

	vector<double> param1{ 1.0, -20., 5., 7 };
	vector<double> param2{ 1.15, 0., 0., 5 };
	vector<double> param3{ 1.3, 75., -75., 10 };

	Gauss = GaussDome(param1);
	GaussHelp = GaussDome(param2);

	for (int i = 0; i < Gauss.size(); i++)
	{
		for (int j = 0; j < Gauss[i].size(); j++)
		{
			Gauss[i][j].value.real += GaussHelp[i][j].value.real;
		}
	}

	GaussHelp = GaussDome(param3);
	for (int i = 0; i < Gauss.size(); i++)
	{
		for (int j = 0; j < Gauss[i].size(); j++)
		{
			Gauss[i][j].value.real += GaussHelp[i][j].value.real;
		}
	}

	/*ofstream out("ПомощникВектор.txt");
	out << "i" << "\t" << "x" << "\t" << "y" << "\t" << "value" << "\t" << "color" << endl;
	for (int i = 0; i < Gauss.size(); i++)
	{
		out << i << "\t" << Gauss[i].x << "\t" << Gauss[i].y << "\t" << Gauss[i].value << "\t" << Gauss[i].color << endl;
	}
	out.close();*/

	return Gauss;
}

CImage CImageFilteringDlg::ImageUpload()
{
	CFileDialog fileDialog(TRUE, NULL, L"*.png;*.bmp;*.jpg;*.jpeg");
	int res = fileDialog.DoModal();
	if (res != IDOK)
		MessageBox(L"Картинка не загружена!", L"Ошибка!", MB_OK || MB_ICONERROR);;

	CImage img;
	CString file_name = fileDialog.GetPathName();
	HRESULT read_img = img.Load(file_name);

	return img;
}

vector<vector<DotsDome>> CImageFilteringDlg::WriteImageToVector(CImage img)
{
	vector<vector<DotsDome>> vec;

	int width = img.GetWidth();
	int height = img.GetHeight();

	vec.resize(width);
	for (int i = 0; i < width; i++) 
	{
		vec[i].resize(height);
	}

	for (int i = 0; i < width; i++) 
	{
		for (int j = 0; j < height; j++) 
		{
			vec[i][j].value.real = img.GetPixel(j, i);
		}
	}

	return vec;
}

//Функция масштаба1 vector<vector<DotsDome>>& solve_buff
void CImageFilteringDlg::Mashtab(vector<vector<DotsDome>>& solve_buff, double* mmin, double* mmax)		//определяем функцию масштабирования
{
	*mmin = *mmax = solve_buff[0][0].value.real;

	for (int i = 0; i < solve_buff.size(); i++)
	{
		for (int j = 0; j < solve_buff[i].size(); j++)
		{
			if (*mmin > solve_buff[i][j].value.real) *mmin = solve_buff[i][j].value.real;
			if (*mmax < solve_buff[i][j].value.real) *mmax = solve_buff[i][j].value.real;
		}
	}
}

//Функция масштаба2 vector<double>& solve_buff
void CImageFilteringDlg::Mashtab(vector<double>& solve_buff, double* mmin, double* mmax)
{
	*mmin = *mmax = solve_buff[0];

	for (int i = 0; i < solve_buff.size(); i++)
	{
		if (*mmin > solve_buff[i]) *mmin = solve_buff[i];
		if (*mmax < solve_buff[i]) *mmax = solve_buff[i];
	}
}

//Функция получения интервала цветов
vector<double> CImageFilteringDlg::GetRangeColors(double min, double max)
{
	vector<double> helpPix;
	double ct = (max - min) / 256;

	for (int i = 0; i < 255; i++)
	{
		helpPix.push_back(ct + i * ct);
	}
	helpPix.push_back(max);

	return helpPix;
}

//Функция получения цвета пикселя
int CImageFilteringDlg::GetColorPixel(double value, vector<double> helpPix)
{
	for (int i = 0; i < helpPix.size(); i++)
	{
		if (value <= helpPix[i]) return i;
	}
}

//Функция заполнение цветом массив1
vector<vector<DotsDome>> CImageFilteringDlg::FillingFieldColor(vector<vector<DotsDome>> vec, vector<double> value)
{
	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			vec[i][j].color = GetColorPixel(vec[i][j].value.real, value);
		}
	}

	return vec;
}

//Функция заполнение цветом массив модуля2
vector<vector<DotsDome>> CImageFilteringDlg::FillingFieldColor(vector<vector<DotsDome>> vec, vector<double> module, vector<double> value)
{
	int ch = 0;
	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			vec[i][j].color = GetColorPixel(module[ch], value);
			ch++;
		}
	}

	return vec;
}

//Функция задающая рандомные числа [0,1]
double CImageFilteringDlg::RandStaff()		//рандомизация для шума
{
	double r = 0;
	for (int i = 1; i <= 12; i++)
	{
		r += ((rand() % 100) / (100 * 1.0) * 2) - 1;		// [-1;1]
	}
	return r / 12;
}

//Функция генерации шума
vector<double> CImageFilteringDlg::GenerationNoise(vector<vector<DotsDome>> Gauss)
{
	//генерация шума
	double d = value_noise / 100.;

	double energ_signal = 0;
	for (int i = 0; i < Gauss.size(); i++)
	{
		for (int j = 0; j < Gauss[i].size(); j++)
		{
			energ_signal += Gauss[i][j].value.real * Gauss[i][j].value.real;
		}
	}

	vector<double> noise;
	int size = Gauss.size() * Gauss[0].size();
	for (int i = 0; i < size; i++)
	{
		noise.push_back(RandStaff());
	}

	double qpsi = 0;
	for (int i = 0; i < noise.size(); i++)
	{
		qpsi += noise[i] * noise[i];
	}

	double alpha = sqrt(d * energ_signal / qpsi);

	vector<double> Shum;
	for (int i = 0; i < noise.size(); i++)
	{
		Shum.push_back(alpha * noise[i]);
	}

	return Shum;
}

//Функция заполнение шумом массив
vector<vector<DotsDome>> CImageFilteringDlg::ImposeNoise(vector<vector<DotsDome>> vec, vector<double> vec_noise)
{
	int ch = 0;
	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			vec[i][j].value.real += vec_noise[ch];
			ch++;
		}
	}

	return vec;
}

//Функция вычисляющая число степени двойки
int CImageFilteringDlg::NumberDegreeTwo(int width_height)
{
	int new_WidthHeight = 1;

	while (new_WidthHeight < width_height)
	{
		new_WidthHeight = new_WidthHeight * 2;
	}

	return new_WidthHeight;
}

//Функция дополнения массива 0
vector<vector<DotsDome>> CImageFilteringDlg::SupplementZero(vector<vector<DotsDome>> vec)
{
	vector<vector<DotsDome>> GaussModelHelp;
	vector<DotsDome> VecHelp;
	GaussModelHelp = vec;

	int newSize_x = NumberDegreeTwo(value_width);
	int newSize_y = NumberDegreeTwo(value_height);

	int differenceSize_x = abs(newSize_x - value_width);
	int differenceSize_y = abs(newSize_y - value_height);

	double GU_X = value_width / 2.;
	double GU_Y = value_height / 2.;

	DotsDome help;
	for (int i = value_width; i < newSize_x; i++)
	{
		help.x = i - GU_X;
		for (int j = 0; j < value_height; j++)
		{
			help.y = j - GU_Y;
			help.value.real = 0.0;
			help.value.image = 0.0;
			VecHelp.push_back(help);
		}

		GaussModelHelp.push_back(VecHelp);
		VecHelp.clear();
	}

	for (int i = 0; i < GaussModelHelp.size(); i++)
	{
		help.x = i - GU_X;
		int ch = 0;
		for (int j = GaussModelHelp[i].size(); j < newSize_y; j++)
		{
			help.y = GU_Y + ch;
			help.value.real = 0.0;
			help.value.image = 0.0;
			GaussModelHelp[i].push_back(help);
			ch++;
		}
	}

	return GaussModelHelp;
}

vector<DotsDome> CImageFilteringDlg::InterpolationVector(vector<DotsDome> vec, int new_size, bool is_width)
{
	int old_size = vec.size();
	vector<DotsDome> new_vec;
	double new_step = ((double)old_size / (double)new_size);
	double coord_stepX = ((double)value_width / (double)new_size);
	double coord_stepY = ((double)value_height / (double)new_size);
	DotsDome new_pixel;
	int ch = 1;

	new_vec.push_back(vec[0]);
	for (double i = new_step; i < old_size; i += new_step)
	{
		int left_bound = (int)i;
		int right_bound = left_bound + 1;

		if (new_size > old_size)
		{
			if (right_bound >= old_size)
			{
				break;
			}
		}
		else
		{
			if (right_bound >= old_size - 1)
			{
				break;
			}
		}

		if (is_width)
		{
			new_pixel.x = vec[0].x + coord_stepX * ch;
			new_pixel.y = vec[0].y;
		}
		else
		{
			new_pixel.x = vec[0].x;
			new_pixel.y = vec[0].y + coord_stepY * ch;
		}

		// Вычисление нового значения реальной части.
		double upper_re = vec[right_bound].value.real - vec[left_bound].value.real;
		double lower = right_bound - left_bound;
		new_pixel.value.real = vec[left_bound].value.real + (i - left_bound) * (upper_re / lower);

		// Вычисление нового значения мнимой части.
		double upper_im = vec[right_bound].value.image - vec[left_bound].value.image;
		new_pixel.value.image = vec[left_bound].value.image + (i - left_bound) * (upper_im / lower);

		new_vec.push_back(new_pixel);
		ch++;
	}

	new_vec.push_back(vec.back());

	return new_vec;
}

//Функция дополнения массива интерполяцией
vector<vector<DotsDome>> CImageFilteringDlg::SupplementInterpolation(vector<vector<DotsDome>> vec)
{
	vector<vector<DotsDome>> GaussModel;
	vector<vector<DotsDome>> GaussModelHelp;
	vector<DotsDome> GaussHelp;
	vector<DotsDome> VecHelp;

	for (int j = 0; j < vec[0].size(); j++)
	{
		for (int i = 0; i < vec.size(); i++)
		{
			GaussHelp.push_back(vec[i][j]);
		}

		int newSize = NumberDegreeTwo(GaussHelp.size());
		VecHelp = InterpolationVector(GaussHelp, newSize, 1);
		GaussHelp.clear();

		GaussModelHelp.push_back(VecHelp);
	}

	for (int j = 0; j < GaussModelHelp[0].size(); j++)
	{
		for (int i = 0; i < GaussModelHelp.size(); i++)
		{
			GaussHelp.push_back(GaussModelHelp[i][j]);
		}

		int newSize = NumberDegreeTwo(GaussHelp.size());
		VecHelp = InterpolationVector(GaussHelp, newSize, 0);
		GaussHelp.clear();

		GaussModel.push_back(VecHelp);
	}

	return GaussModel;
}

//Функция Фурье
vector<complex_number> CImageFilteringDlg::fourea(vector<complex_number> data, int n, int is)
{
	int i, j, istep;
	int m, mmax;
	double r, r1, theta, w_r, w_i, temp_r, temp_i;
	double pi = 3.1415926;

	r = pi * is;
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].real;
			temp_i = data[j].image;
			data[j].real = data[i].real;
			data[j].image = data[i].image;
			data[i].real = temp_r;
			data[i].image = temp_i;
		}
		m = n >> 1;
		while (j >= m) { j -= m; m = (m + 1) / 2; }
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (double)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1 * m;
			w_r = (double)cos((double)theta);
			w_i = (double)sin((double)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r * data[j].real - w_i * data[j].image;
				temp_i = w_r * data[j].image + w_i * data[j].real;
				data[j].real = data[i].real - temp_r;
				data[j].image = data[i].image - temp_i;
				data[i].real += temp_r;
				data[i].image += temp_i;
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i].real /= (double)n;
			data[i].image /= (double)n;
		}

	vector<complex_number> vecFourea = data;
	return vecFourea;
}

//Функция получения спектра
vector<vector<DotsDome>> CImageFilteringDlg::GetSpectrum(vector<vector<DotsDome>> vec)
{
	vector<vector<DotsDome>> GaussSpectrum;
	GaussSpectrum = vec;
	vector<complex_number> GaussSpectrumHelp;
	vector<complex_number> FoureaHelp;

	for (int j = 0; j < GaussSpectrum[0].size(); j++)
	{
		for (int i = 0; i < GaussSpectrum.size(); i++)
		{
			GaussSpectrumHelp.push_back(GaussSpectrum[i][j].value);
		}

		FoureaHelp = fourea(GaussSpectrumHelp, GaussSpectrumHelp.size(), -1);
		GaussSpectrumHelp.clear();

		for (int i = 0; i < vec.size(); i++)
		{
			GaussSpectrum[i][j].value = FoureaHelp[i];
		}
	}

	for (int i = 0; i < GaussSpectrum.size(); i++)
	{
		for (int j = 0; j < GaussSpectrum[i].size(); j++)
		{
			GaussSpectrumHelp.push_back(GaussSpectrum[i][j].value);
		}

		FoureaHelp = fourea(GaussSpectrumHelp, GaussSpectrumHelp.size(), -1);
		GaussSpectrumHelp.clear();

		for (int j = 0; j < GaussSpectrum[i].size(); j++)
		{
			GaussSpectrum[i][j].value = FoureaHelp[j];
		}
	}

	return GaussSpectrum;
}

//Функция получения модуля спектра
vector<double> CImageFilteringDlg::GetModuleSpectrum(vector<vector<DotsDome>> vec)
{
	vector<double> ModuleSpec;

	for (int i = 0; i < vec.size(); i++)
	{
		for (int j = 0; j < vec[i].size(); j++)
		{
			double help = sqrt((vec[i][j].value.real * vec[i][j].value.real) + (vec[i][j].value.image * vec[i][j].value.image));
			ModuleSpec.push_back(help);
		}
	}

	return ModuleSpec;
}

//Функция выделения квадрантов
vector<vector<vector<DotsDome>>> CImageFilteringDlg::HighlightQuadrants(vector<vector<DotsDome>> vec)
{
	vector<vector<vector<DotsDome>>> Quadrants;

	vector<vector<DotsDome>> Quadrant1;
	vector<vector<DotsDome>> Quadrant2;
	vector<vector<DotsDome>> Quadrant3;
	vector<vector<DotsDome>> Quadrant4;

	vector<DotsDome> help1;
	vector<DotsDome> help2;
	vector<DotsDome> help3;
	vector<DotsDome> help4;

	int sizeLeftX = 0;
	int sizeCenterX = vec.size() / 2;
	int sizeRightX = vec.size();

	int sizeLeftY = 0;
	int sizeCenterY = vec[0].size() / 2;
	int sizeRightY = vec[0].size();

	for (int i = 0; i < sizeRightX; i++)
	{
		bool pushback1 = false;
		bool pushback2 = false;
		bool pushback3 = false;
		bool pushback4 = false;

		for (int j = 0; j < sizeRightY; j++)
		{
			//x=i, y=j
			if (i >= sizeCenterX && i < sizeRightX && j >= sizeCenterY && j < sizeRightY)
			{
				//1 четверть
				help1.push_back(vec[i][j]);
				pushback1 = true;
			}

			if (i >= sizeLeftX && i < sizeCenterX && j >= sizeCenterY && j < sizeRightY)
			{
				//2 четверть 
				help2.push_back(vec[i][j]);
				pushback2 = true;
			}

			if (i >= sizeLeftX && i < sizeCenterX && j >= sizeLeftY && j < sizeCenterY)
			{
				//3 четверть 
				help3.push_back(vec[i][j]);
				pushback3 = true;
			}

			if (i >= sizeCenterX && i < sizeRightX && j >= sizeLeftY && j < sizeCenterY)
			{
				//4 четверть
				help4.push_back(vec[i][j]);
				pushback4 = true;
			}
		}

		if (pushback1 == true)
		{
			Quadrant1.push_back(help1);
			help1.clear();
		}

		if (pushback2 == true)
		{
			Quadrant2.push_back(help2);
			help2.clear();
		}

		if (pushback3 == true)
		{
			Quadrant3.push_back(help3);
			help3.clear();
		}

		if (pushback4 == true)
		{
			Quadrant4.push_back(help4);
			help4.clear();
		}
	}

	Quadrants.push_back(Quadrant1);
	Quadrants.push_back(Quadrant2);
	Quadrants.push_back(Quadrant3);
	Quadrants.push_back(Quadrant4);

	return Quadrants;
}

//Функция меняет квадранты местами 1<->3 и 2<->4
vector<vector<DotsDome>> CImageFilteringDlg::SwapQuarters(vector<vector<DotsDome>> vec)
{
	vector<vector<vector<DotsDome>>> Quadrants = HighlightQuadrants(vec);

	vector<vector<DotsDome>> Quadrant1 = Quadrants[0];
	vector<vector<DotsDome>> Quadrant2 = Quadrants[1];
	vector<vector<DotsDome>> Quadrant3 = Quadrants[2];
	vector<vector<DotsDome>> Quadrant4 = Quadrants[3];

	int sizeLeftX = 0;
	int sizeCenterX = vec.size() / 2;
	int sizeRightX = vec.size();

	int sizeLeftY = 0;
	int sizeCenterY = vec[0].size() / 2;
	int sizeRightY = vec[0].size();

	for (int i = 0; i < sizeRightX; i++)
	{
		for (int j = 0; j < sizeRightY; j++)
		{
			if (i >= sizeCenterX && i < sizeRightX && j >= sizeCenterY && j < sizeRightY)
			{
				//1 четверть
				vec[i][j].value = Quadrant3[i - sizeCenterX][j - sizeCenterY].value;
				vec[i][j].color = Quadrant3[i - sizeCenterX][j - sizeCenterY].color;
			}

			if (i >= sizeLeftX && i < sizeCenterX && j >= sizeCenterY && j < sizeRightY)
			{
				//2 четверть 
				vec[i][j].value = Quadrant4[i][j - sizeCenterY].value;
				vec[i][j].color = Quadrant4[i][j - sizeCenterY].color;
			}

			if (i >= sizeLeftX && i < sizeCenterX && j >= sizeLeftY && j < sizeCenterY)
			{
				//3 четверть 
				vec[i][j].value = Quadrant1[i][j].value;
				vec[i][j].color = Quadrant1[i][j].color;
			}

			if (i >= sizeCenterX && i < sizeRightX && j >= sizeLeftY && j < sizeCenterY)
			{
				//4 четверть
				vec[i][j].value = Quadrant2[i - sizeCenterX][j].value;
				vec[i][j].color = Quadrant2[i - sizeCenterX][j].color;
			}
		}
	}

	return vec;
}

//Функция поворачивает квадранты на 90 градусов вправо к максимумам снизу слева
vector<vector<vector<DotsDome>>>  CImageFilteringDlg::TurnQuarter(vector<vector<vector<DotsDome>>> vec)
{
	vector<vector<vector<DotsDome>>> Quater;

	vector<vector<DotsDome>> Quater1 = vec[0];
	vector<vector<DotsDome>> Quater2 = vec[1];
	vector<vector<DotsDome>> Quater3 = vec[2];
	vector<vector<DotsDome>> Quater4 = vec[3];

	vector<vector<DotsDome>> QuaterNew1 = vec[0];
	vector<vector<DotsDome>> QuaterNew2 = vec[1];
	vector<vector<DotsDome>> QuaterNew4 = vec[3];

	// 1 квадрант
	for (int i = 0; i < Quater1.size(); i++) {
		for (int j = 0; j < Quater1[i].size(); j++) {
			QuaterNew1[i][j] = Quater1[Quater1.size() - 1 - i][Quater1[Quater1.size() - 1 - i].size() - 1 - j];
		}
	}

	// 2 квадрант
	for (int i = 0; i < Quater2.size(); i++) {
		for (int j = 0; j < Quater2[i].size(); j++) {
			QuaterNew2[i][j] = Quater2[i][Quater2[Quater2.size() - 1 - i].size() - 1 - j];
		}
	}

	// 4 квадрант
	for (int i = 0; i < Quater4.size(); i++) {
		for (int j = 0; j < Quater4[i].size(); j++) {
			QuaterNew4[i][j] = Quater4[Quater4.size() - 1 - i][j];
		}
	}

	/*vector<DotsDome> help1;
	vector<DotsDome> help2;
	vector<DotsDome> help4;

	int sizeX = Quater1.size();
	int sizeY = Quater1[0].size();*/

	/*public static int[, ] RotateMatrixClockwise(int[, ] oldMatrix)
	{
		int[, ] newMatrix = new int[oldMatrix.GetLength(1), oldMatrix.GetLength(0)];
		int newColumn, newRow = 0;
		for (int oldColumn = oldMatrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
		{
			newColumn = 0;
			for (int oldRow = 0; oldRow < oldMatrix.GetLength(0); oldRow++)
			{
				newMatrix[newRow, newColumn] = oldMatrix[oldRow, oldColumn];
				newColumn++;
			}
			newRow++;
		}
		return newMatrix;
	}*/

	//column=j,row=i
	/*int newColumn, newRow = 0;
	for (int oldColumn = sizeY - 1; oldColumn >= 0; oldColumn--)
	{
		newColumn = 0;
		for (int oldRow = 0; oldRow < sizeX; oldRow++)
		{
			QuaterNew1[newRow][newColumn].color = Quater1[oldRow][oldColumn].color;
			QuaterNew1[newRow][newColumn].value = Quater1[oldRow][oldColumn].value;

			QuaterNew2[newRow][newColumn].color = Quater2[oldRow][oldColumn].color;
			QuaterNew2[newRow][newColumn].value = Quater2[oldRow][oldColumn].value;

			QuaterNew4[newRow][newColumn].color = Quater4[oldRow][oldColumn].color;
			QuaterNew4[newRow][newColumn].value = Quater4[oldRow][oldColumn].value;

			newColumn++;
		}
		newRow++;
	}

	newColumn, newRow = 0;
	for (int oldColumn = sizeY - 1; oldColumn >= 0; oldColumn--)
	{
		newColumn = 0;
		for (int oldRow = 0; oldRow < sizeX; oldRow++)
		{
			Quater1[newRow][newColumn].color = QuaterNew1[oldRow][oldColumn].color;
			Quater1[newRow][newColumn].value = QuaterNew1[oldRow][oldColumn].value;

			Quater2[newRow][newColumn].color = QuaterNew2[oldRow][oldColumn].color;
			Quater2[newRow][newColumn].value = QuaterNew2[oldRow][oldColumn].value;

			newColumn++;
		}
		newRow++;
	}

	newColumn, newRow = 0;
	for (int oldColumn = sizeY - 1; oldColumn >= 0; oldColumn--)
	{
		newColumn = 0;
		for (int oldRow = 0; oldRow < sizeX; oldRow++)
		{
			QuaterNew2[newRow][newColumn].color = Quater2[oldRow][oldColumn].color;
			QuaterNew2[newRow][newColumn].value = Quater2[oldRow][oldColumn].value;

			newColumn++;
		}
		newRow++;
	}*/


	/*for (int i = 0; i < sizeX; ++i)
	{
		for (int j = 0; j < sizeY; ++j)
		{
			QuaterNew1[i][j].color = Quater1[sizeY - j - 1][i].color;
			QuaterNew1[i][j].value = Quater1[sizeY - j - 1][i].value;

			QuaterNew2[i][j].color = Quater2[sizeY - j - 1][i].color;
			QuaterNew2[i][j].value = Quater2[sizeY - j - 1][i].value;

			QuaterNew4[i][j].color = Quater4[sizeY - j - 1][i].color;
			QuaterNew4[i][j].value = Quater4[sizeY - j - 1][i].value;
		}
	}


	for (int i = 0; i < sizeX; ++i)
	{
		for (int j = 0; j < sizeY; ++j)
		{
			Quater1[i][j].color = QuaterNew1[sizeY - j - 1][i].color;
			Quater1[i][j].value = QuaterNew1[sizeY - j - 1][i].value;

			Quater2[i][j].color = QuaterNew2[sizeY - j - 1][i].color;
			Quater2[i][j].value = QuaterNew2[sizeY - j - 1][i].value;
		}
	}


	for (int i = 0; i < sizeX; ++i)
	{
		for (int j = 0; j < sizeY; ++j)
		{
			QuaterNew2[i][j].color = Quater2[sizeY - j - 1][i].color;
			QuaterNew2[i][j].value = Quater2[sizeY - j - 1][i].value;
		}
	}*/



	Quater.push_back(QuaterNew1);
	Quater.push_back(QuaterNew2);
	Quater.push_back(Quater3);
	Quater.push_back(QuaterNew4);

	return Quater;
}

//Функция дающая область фильтрация
vector<double> CImageFilteringDlg::SelectFilteringArea(vector<vector<DotsDome>> vec)
{
	vector<double> Coord;

	double d = value_filter / 100.;
	double energyMax = 0;

	for (int i = 0; i < vec.size(); ++i)
	{
		for (int j = 0; j < vec[i].size(); ++j)
		{
			energyMax += vec[i][j].value.real * vec[i][j].value.real + vec[i][j].value.image * vec[i][j].value.image;
		}
	}

	double energyFilter = energyMax * d;

	vector<vector<vector<DotsDome>>> Quadrant = HighlightQuadrants(vec);
	Quadrant = TurnQuarter(Quadrant);

	vector<vector<DotsDome>> Quadrant1 = Quadrant[0];
	vector<vector<DotsDome>> Quadrant2 = Quadrant[1];
	vector<vector<DotsDome>> Quadrant3 = Quadrant[2];
	vector<vector<DotsDome>> Quadrant4 = Quadrant[3];

	double energyHelp = 0;
	int radius = 1;
	int size = Quadrant1.size();
	double center = 0.0;

	while (energyHelp < energyFilter && radius < size)
	{
		energyHelp = 0;
		for (int i = 0; i < radius; ++i)
		{
			for (int j = 0; j < radius; ++j)
			{
				if (radio_circle.GetCheck() == BST_CHECKED)
				{
					double in_circle_condition = (i - center) * (i - center) + (j - center) * (j - center);
					if (in_circle_condition <= radius * radius)
					{
						energyHelp += Quadrant1[i][j].value.real * Quadrant1[i][j].value.real + Quadrant1[i][j].value.image * Quadrant1[i][j].value.image;
						energyHelp += Quadrant2[i][j].value.real * Quadrant2[i][j].value.real + Quadrant2[i][j].value.image * Quadrant2[i][j].value.image;
						energyHelp += Quadrant3[i][j].value.real * Quadrant3[i][j].value.real + Quadrant3[i][j].value.image * Quadrant3[i][j].value.image;
						energyHelp += Quadrant4[i][j].value.real * Quadrant4[i][j].value.real + Quadrant4[i][j].value.image * Quadrant4[i][j].value.image;
					}
				}

				if (radio_rectangle.GetCheck() == BST_CHECKED)
				{
					if (j <= radius && i <= radius)
					{
						energyHelp += Quadrant1[i][j].value.real * Quadrant1[i][j].value.real + Quadrant1[i][j].value.image * Quadrant1[i][j].value.image;
						energyHelp += Quadrant2[i][j].value.real * Quadrant2[i][j].value.real + Quadrant2[i][j].value.image * Quadrant2[i][j].value.image;
						energyHelp += Quadrant3[i][j].value.real * Quadrant3[i][j].value.real + Quadrant3[i][j].value.image * Quadrant3[i][j].value.image;
						energyHelp += Quadrant4[i][j].value.real * Quadrant4[i][j].value.real + Quadrant4[i][j].value.image * Quadrant4[i][j].value.image;
					}
				}
			}
		}

		radius++;
	}

	int index_i = vec.size() / 2;
	int index_j = vec[0].size() / 2;

	double centerX = vec[index_i][index_j].x - 0.5;
	double centerY = vec[index_i][index_j].y - 0.5;

	double minX = (centerX - radius);
	double minY = (centerY - radius);

	double maxX = (centerX + radius);
	double maxY = (centerY + radius);

	Coord.push_back(minX);
	Coord.push_back(minY);
	Coord.push_back(maxX);
	Coord.push_back(maxY);
	Coord.push_back(radius);

	return Coord;
}

//Функция зануляющая лишнее
vector<vector<DotsDome>> CImageFilteringDlg::NullifyTheExcess(vector<vector<DotsDome>> vec, int radius)
{
	vector<vector<vector<DotsDome>>> Quadrant = HighlightQuadrants(vec);
	Quadrant = TurnQuarter(Quadrant);

	vector<vector<DotsDome>> Quadrant1 = Quadrant[0];
	vector<vector<DotsDome>> Quadrant2 = Quadrant[1];
	vector<vector<DotsDome>> Quadrant3 = Quadrant[2];
	vector<vector<DotsDome>> Quadrant4 = Quadrant[3];

	for (int i = 0; i < Quadrant1.size(); ++i)
	{
		for (int j = 0; j < Quadrant1[i].size(); ++j)
		{
			if (i > radius || j > radius)
			{
				Quadrant1[i][j].value.real = 0.0;
				Quadrant1[i][j].value.image = 0.0;

				Quadrant2[i][j].value.real = 0.0;
				Quadrant2[i][j].value.image = 0.0;

				Quadrant3[i][j].value.real = 0.0;
				Quadrant3[i][j].value.image = 0.0;

				Quadrant4[i][j].value.real = 0.0;
				Quadrant4[i][j].value.image = 0.0;
			}
		}
	}

	Quadrant[0] = Quadrant1;
	Quadrant[1] = Quadrant2;
	Quadrant[2] = Quadrant3;
	Quadrant[3] = Quadrant4;

	Quadrant = TurnQuarter(Quadrant);

	Quadrant1 = Quadrant[0];
	Quadrant2 = Quadrant[1];
	Quadrant3 = Quadrant[2];
	Quadrant4 = Quadrant[3];

	int sizeLeftX = 0;
	int sizeCenterX = vec.size() / 2;
	int sizeRightX = vec.size();

	int sizeLeftY = 0;
	int sizeCenterY = vec[0].size() / 2;
	int sizeRightY = vec[0].size();

	vector<vector<DotsDome>> GaussModelHelp = vec;
	for (int i = 0; i < GaussModelHelp.size(); ++i)
	{
		for (int j = 0; j < GaussModelHelp[i].size(); ++j)
		{
			if (i >= sizeCenterX && i < sizeRightX && j >= sizeCenterY && j < sizeRightY)
			{
				//1 четверть
				GaussModelHelp[i][j] = Quadrant1[i - sizeCenterX][j - sizeCenterY];
			}

			if (i >= sizeLeftX && i < sizeCenterX && j >= sizeCenterY && j < sizeRightY)
			{
				//2 четверть 
				GaussModelHelp[i][j] = Quadrant2[i][j - sizeCenterY];
			}

			if (i >= sizeLeftX && i < sizeCenterX && j >= sizeLeftY && j < sizeCenterY)
			{
				//3 четверть 
				GaussModelHelp[i][j] = Quadrant3[i][j];
			}

			if (i >= sizeCenterX && i < sizeRightX && j >= sizeLeftY && j < sizeCenterY)
			{
				//4 четверть
				GaussModelHelp[i][j] = Quadrant4[i - sizeCenterX][j];
			}
		}
	}

	return GaussModelHelp;
}

//Функция восстановления изображения
vector<vector<DotsDome>> CImageFilteringDlg::RecoverySpectrum(vector<vector<DotsDome>> vec)
{
	vector<vector<DotsDome>> GaussFilter;
	GaussFilter = vec;
	vector<complex_number> GaussSpectrumHelp;
	vector<complex_number> FoureaHelp;

	for (int j = 0; j < GaussFilter[0].size(); j++)
	{
		for (int i = 0; i < GaussFilter.size(); i++)
		{
			GaussSpectrumHelp.push_back(GaussFilter[i][j].value);
		}

		FoureaHelp = fourea(GaussSpectrumHelp, GaussSpectrumHelp.size(), 1);
		GaussSpectrumHelp.clear();

		for (int i = 0; i < vec.size(); i++)
		{
			GaussFilter[i][j].value = FoureaHelp[i];
		}
	}

	for (int i = 0; i < GaussFilter.size(); i++)
	{
		for (int j = 0; j < GaussFilter[i].size(); j++)
		{
			GaussSpectrumHelp.push_back(GaussFilter[i][j].value);
		}

		FoureaHelp = fourea(GaussSpectrumHelp, GaussSpectrumHelp.size(), 1);
		GaussSpectrumHelp.clear();

		for (int j = 0; j < GaussFilter[i].size(); j++)
		{
			GaussFilter[i][j].value = FoureaHelp[j];
		}
	}

	return GaussFilter;
}

//Функция удаляющая дополнение
vector<vector<DotsDome>> CImageFilteringDlg::RemoveUnnecessary(vector<vector<DotsDome>> vec)
{
	vector<vector<DotsDome>> vecNew;
	if (radio_zero.GetCheck() == BST_CHECKED)
	{
		vector<DotsDome> help;

		int sizeX = value_width;
		int sizeY = value_height;

		for (int j = 0; j < sizeX; j++)
		{
			for (int i = 0; i < sizeY; i++)
			{
				help.push_back(vec[i][j]);
			}

			vecNew.push_back(help);
			help.clear();
		}
	}

	if (radio_interpolation.GetCheck() == BST_CHECKED)
	{
		vector<vector<DotsDome>> GaussModelHelp;
		vector<DotsDome> GaussHelp;
		vector<DotsDome> VecHelp;

		for (int j = 0; j < vec[0].size(); j++)
		{
			for (int i = 0; i < vec.size(); i++)
			{
				GaussHelp.push_back(vec[i][j]);
			}

			int newSize = value_width;
			VecHelp = InterpolationVector(GaussHelp, newSize, 1);
			GaussHelp.clear();

			GaussModelHelp.push_back(VecHelp);
		}

		for (int j = 0; j < GaussModelHelp[0].size(); j++)
		{
			for (int i = 0; i < GaussModelHelp.size(); i++)
			{
				GaussHelp.push_back(GaussModelHelp[i][j]);
			}

			int newSize = value_height;
			VecHelp = InterpolationVector(GaussHelp, newSize, 0);
			GaussHelp.clear();

			vecNew.push_back(VecHelp);
		}
	}

	return vecNew;
}

void CImageFilteringDlg::OnBnClickedCalculate()
{
	UpdateData(TRUE);

	if (radio_model.GetCheck())
	{
		//расчет гауссового купола
		GaussModel = GaussCalculate();
	}
	else if (radio_image.GetCheck())
	{
		//расчет гауссового купола
		CImage img = ImageUpload();

		GaussModel = WriteImageToVector(img);

		/*CFileDialog fileDialog(TRUE, NULL, L"*.png;*.bmp;*.jpg;*.jpeg");
		int res = fileDialog.DoModal();
		if (res != IDOK)
			MessageBox(L"Картинка не загружена!", L"Ошибка!", MB_OK || MB_ICONERROR);;

		CImage img;
		CString file_name = fileDialog.GetPathName();
		HRESULT read_img = img.Load(file_name);

		int width = img.GetWidth();
		int height = img.GetHeight();

		GaussModel.resize(width);
		for (int i = 0; i < width; i++)
		{
			GaussModel[i].resize(height);
		}

		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				GaussModel[width - 1 - i][j].value.real = img.GetPixel(j, i);
			}
		}*/
	
	}
	else if(!radio_image.GetCheck())
	{
		MessageBox(L"Выберите, пожалуйста, тип изображения: модель или картинка!", L"Ошибка!", MB_OK || MB_ICONERROR);
		return;
	}
	
	double min, max;
	Mashtab(GaussModel, &min, &max); //находим макс и мин значение в массиве
	vector<double> range_value = GetRangeColors(min, max); //создаем диапазон значений для каждого цвета
	GaussModel = FillingFieldColor(GaussModel, range_value); //заполняем исходный вектор с куполом цветами

	//расчет купола+шума
	vector<double> vecNoise = GenerationNoise(GaussModel); //генерируем шум
	GaussModelNoise = GaussModel; //приравниваем старый вектор к новому
	GaussModelNoise = ImposeNoise(GaussModelNoise, vecNoise); //добавляем шум к исходному вектору
	Mashtab(GaussModelNoise, &min, &max); //аналогично предыдущему пункту
	vector<double> range_valueN = GetRangeColors(min, max);
	GaussModelNoise = FillingFieldColor(GaussModelNoise, range_valueN);

	//дополнение до степени 2 нулями
	vector<vector<DotsDome>> GaussModelHelp;
	if (radio_zero.GetCheck() == BST_CHECKED)
	{
		GaussModelHelp = SupplementZero(GaussModelNoise);
	}

	//дополнение до степени 2 с помощью интерполяции
	if (radio_interpolation.GetCheck() == BST_CHECKED)
	{
		GaussModelHelp = SupplementInterpolation(GaussModelNoise);
	}

	//вычисление спектра модели
	GaussModelHelp = GetSpectrum(GaussModelHelp);

	//вычисление модуля спектра
	vector<double> GaussModuleSpectrum;
	if (radio_line.GetCheck() == BST_CHECKED)
	{
		GaussModuleSpectrum = GetModuleSpectrum(GaussModelHelp);
	}

	Mashtab(GaussModuleSpectrum, &min, &max);
	vector<double> range_valueS = GetRangeColors(min, max);
	GaussModelHelp = FillingFieldColor(GaussModelHelp, GaussModuleSpectrum, range_valueS);

	//меняем квадранты местами 1<->3 и 2<->4
	GaussModelSpectrum = SwapQuarters(GaussModelHelp);

	if (radio_circle.GetCheck() == BST_CHECKED || radio_rectangle.GetCheck() == BST_CHECKED)
	{
		//находим область фильтрации, которую не зануляем
		CoordWindow = SelectFilteringArea(GaussModelHelp);
	}

	//зануляем лишнее и восстанавливаем
	vector<vector<DotsDome>> GaussFilter = NullifyTheExcess(GaussModelHelp, CoordWindow[4]);
	//GaussModelFilter = GaussModelHelp;
	GaussFilter = RecoverySpectrum(GaussFilter);
	GaussModelFilter = RemoveUnnecessary(GaussFilter);

	Mashtab(GaussModelFilter, &min, &max);
	vector<double> range_valueF = GetRangeColors(min, max);
	GaussModelFilter = FillingFieldColor(GaussModelFilter, range_valueF);

	double chislitel = 0;
	double znamenatel = 0;
	for (int i = 0; i < GaussModel.size(); ++i)
	{
		for (int j = 0; j < GaussModel[0].size(); ++j)
		{
			chislitel += (GaussModel[i][j].color- GaussModelFilter[i][j].color) * (GaussModel[i][j].color - GaussModelFilter[i][j].color);
			znamenatel += GaussModel[i][j].color * GaussModel[i][j].color;
		}
	}

	value_SKO = sqrt(chislitel / znamenatel);

	UpdateData(FALSE);
}

void CImageFilteringDlg::OnBnClickedDischarge()
{
	GaussModel.clear();
	GaussModelNoise.clear();
	GaussModelSpectrum.clear();
	GaussModelFilter.clear();
	CoordWindow.clear();
}

void CImageFilteringDlg::OnBnClickedDrawimage()
{
	if (!GaussModel.empty())
	{
		DrawGauss(GaussModel, PicDcImage, PicImage);
	}
}

void CImageFilteringDlg::OnBnClickedDrawnoise()
{
	if (!GaussModelNoise.empty())
	{
		DrawGaussNoise(GaussModelNoise, PicDcNoise, PicNoise);
	}
}

void CImageFilteringDlg::OnBnClickedDrawspectrum()
{
	if (!GaussModelNoise.empty())
	{
		DrawGaussSpectrum(GaussModelSpectrum, CoordWindow, PicDcSpectrum, PicSpectrum);
	}
}

void CImageFilteringDlg::OnBnClickedDrawfilter()
{
	if (!GaussModelFilter.empty())
	{
		DrawGaussFilter(GaussModelFilter, PicDcFilter, PicFilter);
	}
}

//vector<vector<vector<DotsDome>>> Quadrant = HighlightQuadrants(GaussModelHelp);
//Quadrant = TurnQuarter(Quadrant);

//vector<vector<DotsDome>> Quadrant1 = Quadrant[0];
//vector<vector<DotsDome>> Quadrant2 = Quadrant[1];
//vector<vector<DotsDome>> Quadrant3 = Quadrant[2];
//vector<vector<DotsDome>> Quadrant4 = Quadrant[3];

//int sizeLeftX = 0;
//int sizeCenterX = GaussModelHelp[0].size() / 2;
//int sizeRightX = GaussModelHelp[0].size();

//int sizeLeftY = 0;
//int sizeCenterY = GaussModelHelp.size() / 2;
//int sizeRightY = GaussModelHelp.size();

//for (int i = 0; i < GaussModelHelp.size(); ++i)
//{
//	for (int j = 0; j < GaussModelHelp[0].size(); ++j)
//	{
//		if (i >= sizeCenterY && i < sizeRightY && j >= sizeCenterX && j < sizeRightX)
//		{
//			//1 четверть
//			GaussModelHelp[i][j] = Quadrant1[i - sizeCenterY][j - sizeCenterX];
//		}

//		if (i >= sizeLeftY && i < sizeCenterY && j >= sizeCenterX && j < sizeRightX)
//		{
//			//2 четверть 
//			GaussModelHelp[i][j] = Quadrant2[i][j - sizeCenterX];
//		}

//		if (i >= sizeLeftY && i < sizeCenterY && j >= sizeLeftX && j < sizeCenterX)
//		{
//			//3 четверть 
//			GaussModelHelp[i][j] = Quadrant3[i][j];
//		}

//		if (i >= sizeCenterY && i < sizeRightY && j >= sizeLeftX && j < sizeCenterX)
//		{
//			//4 четверть
//			GaussModelHelp[i][j] = Quadrant4[i - sizeCenterY][j];
//		}
//	}
//}

//GaussModelSpectrum = GaussModelHelp;

//ofstream out("ПомощникВектор.txt");
//out << "value.real1" << "\t" << "value.real2" << "\t" << "value.image1" << "\t" << "value.image2" << endl;
//int ch = 0;
//for (int i = 0; i < GaussModelSpectrum.size(); i++)
//{
//	for (int j = 0; j < GaussModelSpectrum[i].size(); j++)
//	{
//		/*out << i << "\t" << GaussModelHelp[i][j].value.real << "\t" << GaussModelHelp[i][j].value.real << "\t"
//						 << GaussModelSpectrum[i][j].value.image << "\t" << GaussModelSpectrum[i][j].value.real << endl;*/
//		out << ch << "\t" << GaussModelSpectrum[i][j].value.real << "\t" << GaussModelSpectrum[i][j].value.image
//			<< "\t" << GaussModuleSpectrum[ch] << endl;
//		ch++;
//	}
//}
//out.close();